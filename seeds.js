var mongoose    = require('mongoose');
var Campamento  = require("./models/campamento");
var Comentario  = require("./models/comentario");
var User        = require("./models/user");

var datos = [
    {
        nombre: "Grupo Waingunga",
        precio: "5",
        imagen: "https://images.unsplash.com/photo-1445308394109-4ec2920981b1?dpr=1&auto=compress,format&fit=crop&w=767&h=510&q=80&cs=tinysrgb&crop=",
        descripcion: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laboriosam ipsam earum at! Blanditiis fugiat iusto soluta aspernatur itaque voluptatum, non provident repellat esse quam obcaecati aliquid. Aut aliquid laboriosam tenetur."
    },
    {
        nombre: "Flecha Extreme",
        precio: "9",
        imagen: "https://images.unsplash.com/photo-1492648272180-61e45a8d98a7?dpr=1&auto=compress,format&fit=crop&w=767&h=511&q=80&cs=tinysrgb&crop=",
        descripcion: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laboriosam ipsam earum at! Blanditiis fugiat iusto soluta aspernatur itaque voluptatum, non provident repellat esse quam obcaecati aliquid. Aut aliquid laboriosam tenetur."
    },
    {
        nombre: "LCF Kids Club",
        precio: "10",
        imagen: "https://images.unsplash.com/photo-1487730116645-74489c95b41b?dpr=1&auto=compress,format&fit=crop&w=767&h=511&q=80&cs=tinysrgb&crop=",
        descripcion: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laboriosam ipsam earum at! Blanditiis fugiat iusto soluta aspernatur itaque voluptatum, non provident repellat esse quam obcaecati aliquid. Aut aliquid laboriosam tenetur."
    }
]

function seedDB() {
    Campamento.remove({}, function(err) {
        if(err) {
            console.log(err);
        } else {
            console.log("Campamentos eliminados!");
            User.remove({}, function(err) {
                if(err) {
                    console.log(err);
                } else {
                    console.log("Todos los usuarios eliminados");
                    var nuevoUsuario = new User({username: "test"});
                    User.register(nuevoUsuario, "test", function(err, user) {
                        if(err) {
                            console.log(err);
                        } else {
                            console.log("Creado usuario test:test");
                            datos.forEach(function(dato) {
                                dato.autor = {
                                    id: user._id,
                                    username: user.username
                                }
                                Campamento.create(dato, function(err, datoAgr) {
                                    if(err) {
                                        console.log(err);
                                    } else {
                                        console.log("Campamento agregado!");
                                        Comentario.create(
                                            {
                                                texto: "Comentario del campamento",
                                                autor: {
                                                    id: user._id,
                                                    username: user.username
                                                }
                                            }, function(err, comentario) {
                                                if(err) {
                                                    console.log(err);
                                                } else {
                                                    datoAgr.comentarios.push(comentario);
                                                    datoAgr.save();
                                                    console.log("Creado nuevo comentario");
                                                }
                                            }
                                        );
                                    }
                                });
                            });
                        }
                    });
                }
            })
        }
    });
}

module.exports = seedDB;
