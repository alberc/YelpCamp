var mongoose = require('mongoose');

var comentarioSchema = mongoose.Schema({
    texto: String,
    autor: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User"
        },
        username: String
    }
}, 
{
    usePushEach: true
});

module.exports = mongoose.model("Comentario", comentarioSchema);