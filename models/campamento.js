var mongoose = require("mongoose");

var campamentoSchema = new mongoose.Schema({
    nombre: String,
    precio: String,
    imagen: String,
    descripcion: String,
    autor: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User"
        },
        username: String
    },
    comentarios: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Comentario"
        }
    ]
}, 
{
    usePushEach: true
});

module.exports = mongoose.model("Campamento", campamentoSchema);