var express         = require("express"),
    app             = express(),
    dotenv          = require('dotenv').config(),
    bodyParser      = require('body-parser'),
    mongoose        = require('mongoose'),
    flash           = require("connect-flash"),
    passport        = require("passport"),
    LocalStrategy   = require("passport-local"),
    methodOverride  = require("method-override"),
    Campamento      = require("./models/campamento"),
    Comentario      = require("./models/comentario"),
    User            = require("./models/user"),
    seedDB          = require("./seeds");

var comentariosRoutes   = require("./routes/comentarios"),
    campamentosRoutes   = require("./routes/campamentos"),
    indexRoutes         = require("./routes/index");

seedDB();
mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI, {
  keepAlive: true,
  reconnectTries: Number.MAX_VALUE,
  useMongoClient: true
});

app.use(bodyParser.urlencoded({ extended: true }));
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.use(methodOverride("_method"));
app.use(flash());

app.use(require("express-session")({
    secret: process.env.SECRET,
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use(function(req, res, next) {
    res.locals.usuarioActual = req.user;
    res.locals.error = req.flash("error");
    res.locals.exito = req.flash("exito");
    next();
});

app.use("/", indexRoutes);
app.use("/campamentos", campamentosRoutes);
app.use("/campamentos/:id/comentarios", comentariosRoutes);

app.listen(process.env.PORT, function() {
    console.log("Servidor iniciado!");
});