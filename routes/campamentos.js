var express = require("express");
var router = express.Router();
var Campamento = require("../models/campamento");
var middleware = require("../middleware");

router.get("/", function(req, res) {
    Campamento.find({}, function(err, listaCampamentos) {
        if(err) {
            console.log(err)
        } else {
            res.render("campamentos/index", {campamentos:listaCampamentos});
        }
    });
});

router.post("/", middleware.isLoggedIn, function(req, res) {
    var nombre = req.body.nombre;
    var precio = req.body.precio;
    var imagen = req.body.imagen;
    var descripcion = req.body.descripcion;
    var autor = {
        id: req.user._id,
        username: req.user.username
    };
    var campamentoNuevo = {
        nombre: nombre,
        precio: precio,
        imagen: imagen,
        descripcion: descripcion,
        autor: autor
    };
    Campamento.create(campamentoNuevo, function(err, campamentoNuevo) {
        if(err) {
            console.log(err)
        } else {
            res.redirect("/campamentos");
        }
    });
});

router.get("/new", middleware.isLoggedIn, function(req, res) {
    res.render("campamentos/new");
});

router.get("/:id", function(req, res) {
    Campamento.findById(req.params.id).populate("comentarios").exec(function(err, campamentoEnc) {
        if(err) {
            console.log(err);
        } else {
            res.render("campamentos/show", {campamento: campamentoEnc});
        }
    });
});

router.get("/:id/edit", middleware.comprobarAutorCamp, function(req, res) {
        Campamento.findById(req.params.id, function(err, campamentoEnc) {
            res.render("campamentos/edit", {campamento: campamentoEnc});
        });
});

router.put("/:id", middleware.comprobarAutorCamp, function(req, res) {
    Campamento.findByIdAndUpdate(req.params.id, req.body.campamento, function(err, campamentoEnc) {
        if(err) {
            res.redirect("/campamentos");
        } else {
            res.redirect("/campamentos/" + req.params.id);
        }
    });
});

router.delete("/:id", middleware.comprobarAutorCamp, function(req, res) {
    Campamento.findByIdAndRemove(req.params.id, function(err) {
        if(err) {
            res.redirect("/campamentos");
        } else {
            res.redirect("/campamentos");
        }
    })
});

module.exports = router;