var express = require("express");
var router = express.Router();
var passport = require("passport");
var User = require("../models/user");

router.get("/", function(req, res) {
    res.render("landing");
});

router.get("/registro", function(req, res) {
    res.render("registro");
});

router.post("/registro", function(req, res) {
    var nuevoUsuario = new User({username: req.body.username});
    User.register(nuevoUsuario, req.body.password, function(err, user) {
        if(err) {
            console.log(err);
            return res.render("registro", {error: err.message});
        }
        passport.authenticate("local")(req, res, function(){
            req.flash("exito", "Bienvenido a YelpCamp " + user.username);
            res.redirect("/campamentos");
        });
    });
});

router.get("/login", function(req, res) {
    res.render("login");
});

router.post("/login", passport.authenticate("local", 
    {
        successRedirect: "/campamentos",
        failureRedirect: "/login",
        failureFlash: true
    }), function(req, res) {
});

router.get("/logout", function(req, res) {
    req.logout();
    req.flash("exito", "Sesion cerrada!");
    res.redirect("back");
});

module.exports = router;