var express = require("express");
var router = express.Router({mergeParams: true});
var Campamento = require("../models/campamento");
var Comentario = require("../models/comentario");
var middleware = require("../middleware");

router.get("/new", middleware.isLoggedIn, function(req, res) {
    Campamento.findById(req.params.id, function(err, campamentoEnc) {
        if(err) {
            console.log(err);
        } else {
            res.render("comentarios/new", {campamento: campamentoEnc});
        }
    });
});

router.post("/", middleware.isLoggedIn, function(req, res) {
    Campamento.findById(req.params.id, function(err, campamentoEnc) {
        if(err) {
            console.log(err);
        } else {
            Comentario.create(req.body.comentario, function(err, comentario) {
                if(err) {
                    req.flash("error", "Ocurrió un error");
                    console.log(err);
                } else {
                    comentario.autor.id = req.user._id;
                    comentario.autor.username = req.user.username;
                    comentario.save();
                    campamentoEnc.comentarios.push(comentario);
                    campamentoEnc.save();
                    req.flash("exito", "Comentario enviado!");
                    res.redirect("/campamentos/" + campamentoEnc._id)
                }
            });
        }
    });
});

router.get("/:comentario_id/edit", middleware.comprobarAutorCom, function(req, res) {
    Comentario.findById(req.params.comentario_id, function(err, comentarioEnc) {
        if(err) {
            res.redirect("back");
        } else {
            res.render("comentarios/edit", {campamento_id: req.params.id, comentario: comentarioEnc});
        }
    });
});

router.put("/:comentario_id", middleware.comprobarAutorCom, function(req, res) {
    Comentario.findByIdAndUpdate(req.params.comentario_id, req.body.comentario, function(err, comentarioEnc) {
        if(err) {
            res.redirect("back");
        } else {
            res.redirect("/campamentos/" + req.params.id);
        }
    });
});

router.delete("/:comentario_id", middleware.comprobarAutorCom, function(req, res) {
    Comentario.findByIdAndRemove(req.params.comentario_id, function(err) {
        if(err) {
            res.redirect("back");
        } else {
            req.flash("exito", "Comentario eliminado!");
            res.redirect("/campamentos/" + req.params.id);
        }
    });
});

module.exports = router;