var middlewareObj = {};
var Campamento = require("../models/campamento");
var Comentario = require("../models/comentario");

middlewareObj.comprobarAutorCamp = function(req, res, next) {
    if(req.isAuthenticated()) {
        Campamento.findById(req.params.id, function(err, campamentoEnc) {
            if(err) {
                req.flash("error", "Campamento no encontrado");
                res.redirect("back");
            } else {
                if(campamentoEnc.autor.id.equals(req.user._id)) {
                    next();
                } else {
                    req.flash("error", "No tienes permisos suficientes");
                    res.redirect("back");
                }
            }
        })
    } else {
        req.flash("error", "Debes iniciar sesión");
        res.redirect("back");
    }
}

middlewareObj.comprobarAutorCom = function(req, res, next) {
    if(req.isAuthenticated()) {
        Comentario.findById(req.params.comentario_id, function(err, comentarioEnc) {
            if(err) {
                res.redirect("back");
            } else {
                if(comentarioEnc.autor.id.equals(req.user._id)) {
                    next();
                } else {
                    req.flash("error", "No tienes permisos suficientes");
                    res.redirect("back");
                }
            }
        })
    } else {
        req.flash("error", "Debes iniciar sesión");
        res.redirect("back");
    }
}

middlewareObj.isLoggedIn = function (req, res, next) {
    if(req.isAuthenticated()) {
        return next();
    }
    req.flash("error", "Debes iniciar sesión");
    res.redirect("/login");
}

module.exports = middlewareObj;